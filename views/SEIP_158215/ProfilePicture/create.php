<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

    if(!isset($_SESSION)){
        session_start();
    }
    if(!isset($msg)){
        $msg = Message::getMessage();
    }
    else{
        $msg = "";
    }
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Atomic Projects:Profile Picture</title>
    <link rel="stylesheet" href="../../../resource/css/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script src="../../../resource/js/jquery.min.js"></script>
    <script src="../../../resource/js/login.js"></script>
    <script src="../../../resource/js/modernizr.custom.js"></script>
    <!---tabs-->
    <script type="text/javascript" src="../../../resource/js/JFCore.js"></script>
    <script type="text/javascript">
        (function() {
            JC.init({
                domainKey: ''
            });
        })();
    </script>
</head>
<body>
<div class="wrap">
    <div class="main">
        <!---start-nav---->
        <div class="nav">
            <!---start-top-nav---->
            <div class="top-nav">
                <ul class="menu2">
                    <li>
                        <a  class="nav-icon" href=""> </a>
                        <ul>
                            <li><a class="root-nav1" href="create.php">Add New</a></li>
                            <li><a class="root-nav2" href="read.php">Active List</a></li>
                            <li><a class="root-nav3" href="">Trash List</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!---end-top-nav---->
            <!---start-text---->
            <div class="text">
                <h2>Profile Picture</h2>
            </div>
            <!---end-text---->
            <!-- start login_box -->
            <div class="login_box">
                <div id="loginContainer">
                    <a href="#" id="loginButton" class=""><span></span></a>
                    <div id="loginBox" style="display: none;">
                        <form id="loginForm">
                            <fieldset id="body">
                                <fieldset>
                                    <p class="info"><b>Atomic Project</b>
                                    <p class="info"><b>Submitted By Shusmoy Barua Pranto 158215 Batch 45</b>
                                    </p>
                                </fieldset>

                            </fieldset>
                        </form>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <!-- end_login_box -->
        </div>
        <!---end-nav---->
        <div class="clear"></div>
        <!---start-camera---->
        <div class="camera">
            <div class="image">
                <img src="../../../resource/images/profile.png" alt="">
            </div>
        </div>
        <div class="clear"> </div>
        <!---end-camera---->
        <!----start-tabs--->
        <div class="row">
            <div class="grid_12 columns">
                <div class="tab style-1">
                    <dl>

                    </dl>
                    <ul>
                        <li class="active">
                            <form action="store.php" method="post" enctype="multipart/form-data">
                                <div class="form">
                                    <input type="text" class="textbox" placeholder="Enter Your Name" name="name">

                                    <input type="file" class="textbox" name="picture">

                                    <p>
                                        <input type="submit">
                                </p>
                                </div>
                            </form>
                        </li>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!----end-tabs--->
    </div>
</div>
<div class="clear"> </div>
</body>

    <script src="../../../resource/bootstrap-3.3.7/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script>
        jQuery(function($){
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
        })
    </script>
</body>
</html>