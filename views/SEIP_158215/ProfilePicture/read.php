<?php
require_once("../../../vendor/autoload.php");

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$allData = $objProfilePicture->index();

use App\Message\Message;
use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}
if(!isset($msg)){
    $msg = Message::getMessage();
}
else{
    $msg = "";
}

/*######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objProfilePicture->indexPagination($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################

*/?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender Active List</title>
    <link rel="stylesheet" href="../../../resource/css/Styles1.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/css/styles.css">
</head>
<body>
<div class="menu">
    <ul class="nav nav-pills nav-justified">
        <li role="presentation"><a href="create.php">Home</a></li>
        <li role="presentation"><a href="create.php">Add New</a></li>
        <li role="presentation"  class="active"><a href="read.php">Active List</a></li>
        <li role="presentation"><a href="trashed.php">Trashed List</a></li>
    </ul>
</div>

<div class="row">
    <div class="grid_12 columns">

        <dl>

        </dl>
        <div class="message text-center">
            <h3><?php echo $msg;?></h3>
        </div>
        <table class="w3-table w3-striped w3-bordered w3-border">
            <thead>
            <tr class="w3-red">
                <th><b>SL No</b></th>
                <th><b>ID</b></th>
                <th><b>Name</b></th>
                <th><b>Picture Name</b></th>
                <th><b>Profile Picture</b></th>
                <th margin-right="20px"><center><b>Actions<b></center></th>
            </tr>

            <?php
            $Serial = 1;
            foreach($allData as $oneData){
                echo "
                    <tr class='w3-blue'>
                        <td><b>$Serial<b></td>
                        <td><b>$oneData->id<b></td>
                        <td><b>$oneData->name<b></td>
                        <td><b>$oneData->pictureName<b></td>
                        <td><img class='img img-thumbnail' src='UploadedPicture/$oneData->pictureName' alt=''></td>
                        <td>
                            <a href='view.php?id=$oneData->id' class='w3-btn w3-green w3-border w3-round-xlarge'>View</a>
                            <a href='edit.php?id=$oneData->id&btn=set' class='w3-btn w3-green w3-border w3-round-xlarge'>Edit</a>
                            <a href='trash.php?id=$oneData->id' class='w3-btn w3-green w3-border w3-round-xlarge'>Trash</a>
                            <a href='delete.php?id=$oneData->id&btn=read1' class='w3-btn w3-green w3-border w3-round-xlarge'>Delete</a>
                        </td>
                    </tr>
                ";
                $Serial++;
            }
            ?>
        </table>
    </div>
</div>
</div>    <script src="../../../resource/bootstrap-3.3.7/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script>
        jQuery(function($){
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
        })
    </script>
</body>
</html>