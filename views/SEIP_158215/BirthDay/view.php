<?php
require_once("../../../vendor/autoload.php");

$objBirthDay = new \App\BirthDay\BirthDay();

$objBirthDay->setData($_GET);

$oneData = $objBirthDay->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birth Day Active List</title>
    <link rel="stylesheet" href="../../../resource/css/styles.css">
    <link rel="stylesheet" href="../../../resource/css/styles1.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation"><a href="create.php">Add New</a></li>
            <li role="presentation"  class="active"><a href="read.php">Active List</a></li>
            <li role="presentation"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>

        <div class="row">
			<div class="grid_12 columns">
				<div class="tab style-1">
			    					<dl>
			 			             	 
			    					</dl>
                <table class="w3-table w3-striped w3-bordered w3-border">
                    <tr class="w3-red">
                      <th width="25%"><b>Info</b></th>
                      <th width=25%><b>Value</b></th>
                    </tr>
                    <tr class="w3-blue">
                        <td>ID:</td>
                        <td><?php echo $oneData->id;?></td>
                    </tr>
                    <tr class="w3-grey">
                        <td>Name:</td>
                        <td><?php echo $oneData->name;?></td>
                    </tr class="w3-blue">
                    <tr>
                        <td>BirthDay:</td>
                        <td><?php echo $oneData->birthDate;?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="footer">
            <h4>Copyright &copy; 2017 - Ashok Barua</h4>
        </div>
    </div>
</div>
</body>
</html>