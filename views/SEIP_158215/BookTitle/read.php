<?php
require_once("../../../vendor/autoload.php");

$objBookTitle = new \App\BookTitle\BookTitle;

$allData = $objBookTitle->index();

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
if(!isset($msg)){
    $msg = Message::getMessage();
}
else{
    $msg = "";
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Active List</title>
    <link rel="stylesheet" href="../../../resource/css/styles.css">
    <link rel="stylesheet" href="../../../resource/css/styles1.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.css">
</head>
<body>
    <div class="menu">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation"><a href="../index.php">Home</a></li>
            <li role="presentation"><a href="create.php">Add New</a></li>
            <li role="presentation"  class="active"><a href="read.php">Active List</a></li>
            <li role="presentation"><a href="trashed.php">Trashed List</a></li>
        </ul>
    </div>

        <div class="row">
			<div class="grid_12 columns">
				<div class="tab style-1">
			    					<dl>
                                        <div class="message text-center">
                                            <h3><?php echo $msg;?></h3>
                                        </div>
			    					</dl>
<table class="w3-table w3-striped w3-bordered w3-border">
<thead>
<tr class="w3-red">
  <th width="10%"><b>SL No</b></th>
  <th width="10%"><b>ID</b></th>
  <th width="25%"><b>Book Name</b></th>
  <th width=25%><b>Author Name</b></th>
    <th margin-right="20px"><center><b>Actions<b></center></th>
</tr>
               
                <?php
                $Serial = 1;
                foreach($allData as $oneData){
                    echo "
                    <tr class='w3-blue'>
                        <td><b>$Serial<b></td>
                        <td><b>$oneData->id<b></td>
                        <td><b>$oneData->book_name<b></td>
                        <td><b>$oneData->author_name<b></td>
                        <td>
                            <a href='view.php?id=$oneData->id' class='w3-btn w3-green w3-border w3-round-xlarge'>View</a>
                            <a href='edit.php?id=$oneData->id&btn=set' class='w3-btn w3-green w3-border w3-round-xlarge'>Edit</a>
                            <a href='trash.php?id=$oneData->id' class='w3-btn w3-green w3-border w3-round-xlarge'>Trash</a>
                            <a href='delete.php?id=$oneData->id&btn=read1' class='w3-btn w3-green w3-border w3-round-xlarge'>Delete</a>
                        </td>
                    </tr>
                ";
                    $Serial++;
                }
                ?>
            </table>
        </div>
    </div>
</div>
    <script src="../../../resource/bootstrap-3.3.7/js/jquery.min.js"></script>
    <script src="../../../resource/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script>
        jQuery(function($){
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
            $('.message').fadeIn(550);
            $('.message').fadeOut(550);
        })
    </script>
</body>
</html>