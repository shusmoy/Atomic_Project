<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

    if(!isset($_SESSION)){
        session_start();
    }
    if(!isset($msg)){
        $msg = Message::getMessage();
    }
    else{
        $msg = "";
    }
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Atomic Project:City Selection</title>
    <link rel="stylesheet" href="../../../resource/css/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script src="../../../resource/js/jquery.min.js"></script>
    <script src="../../../resource/js/login.js"></script>
    <script src="../../../resource/js/modernizr.custom.js"></script>
    <!---tabs-->
    <script type="text/javascript" src="../../../resource/js/JFCore.js"></script>
    <script type="text/javascript">
        (function() {
            JC.init({
                domainKey: ''
            });
        })();
    </script>
</head>
<body>
<div class="wrap">
    <div class="main">
        <!---start-nav---->
        <div class="nav">
            <!---start-top-nav---->
            <div class="top-nav">
                <ul class="menu2">
                    <li>
                        <a  class="nav-icon" href=""> </a>
                        <ul>
                            <li><a class="root-nav1" href="create.php">Add New</a></li>
                            <li><a class="root-nav2" href="read.php">Active List</a></li>
                            <li><a class="root-nav3" href="">Trash List</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!---end-top-nav---->
            <!---start-text---->
            <div class="text">
                <h2>City Selection</h2>
            </div>
            <!---end-text---->
            <!-- start login_box -->
            <div class="login_box">
                <div id="loginContainer">
                    <a href="#" id="loginButton" class=""><span></span></a>
                    <div id="loginBox" style="display: none;">
                        <form id="loginForm">
                            <fieldset id="body">
                                <fieldset>
                                    <p class="info"><b>Atomic Project</b>
                                    <p class="info"><b>Submitted By Shusmoy Barua Pranto 158215 Batch 45</b>
                                    </p>
                                </fieldset>

                            </fieldset>
                        </form>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <!-- end_login_box -->
        </div>
        <!---end-nav---->
        <div class="clear"></div>
        <div class="notification">
            <div class="message text-center">
                <h3><?php echo $msg;?></h3>
            </div>
        <!---start-camera---->
        <div class="camera">
            <div class="image">
                <img src="../../../resource/images/city.png" alt="">
            </div>
        </div>
        <div class="clear"> </div>
        <!---end-camera---->
        <!----start-tabs--->
        <div class="row">
            <div class="grid_12 columns">
                <div class="tab style-1">
                    <dl>

                    </dl>
                    <ul>
                        <li class="active">
                            <form action="store.php" method="post">
                            <div class="form">
                                <input type="text" class="textbox" placeholder="Enter Your Name" name="name">
                                <select name="city">
                                    <option>Choice Your City</option>
                                    <option value="Dhaka">Dhaka</option>
                                    <option value="Chittagong">Chittagong</option>
                                    <option value="Khulna">Khulna</option>
                                    <option value="Rajshahi">Rajshahi</option>
                                    <option value="Sylhet">Sylhet</option>
                                    <option value="Rongpur">Rongpur</option>
                                    <option value="Cox's Bazar">Cox's Bazar</option>
                                    <option value="Jessor">Jessor</option>
                                </select>
                                <p>
                                    <input type="submit" value="Submit">
                            </div>
                                </form>
                        </li>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!----end-tabs--->
    </div>
</div>
<div class="clear"> </div>
</body>
</html>
